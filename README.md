# Bot #NoAlPadrón

Este bot envía un correo con comentarios para la CNDH con solo
hacer RT. El nombre del destinario es el nombre de usuario de
TW.

## Uso

1. Coloca las llaves y tokens (`client`) en `get_users.rb`.
2. Coloca el ID del tweet (`$tweet`) al que se tiene que hacer RT en
   `get_users.rb`.
3. Configura el servidor de correos (`Mail.defaults`) en `send-emails.rb`.
3. Coloca este repo en el servidor.
4. Configura un trabajo CRON o similar para que cada cierto tiempo obtenga los
   usuarios que han hecho RT y otro cada tanto para que envíe los correos.

## Notas

### Almacenamiento de datos

Para evitar duplicados, se van almacenando listas de usuarios:

* `./src/users_new.txt` añade los usuarios que han hecho RT.
* `./src/users_old.txt` almacena los usuarios que ya mandaron correo.

Solo manda correo de los usuarios que aún no pertenecen a `users_old.txt`.
Una vez que se manda, el usuario pasa a pertenecer a esta lista
para evitar envíos duplicados.

### Aleatorización del texto

El texto del asunto (`./src/subjects.txt`) y cuerpo (`./src/body.html`)
del correo no es siempre el mismo.

La función `random_text` en `send-email.rb` elige de manera aleatoria
una opción de varias posibles acotadas con la sintaxis `@[opt1,opt2,opt3]`.

Entre más `@[n]` mayor aleatoriedad.
